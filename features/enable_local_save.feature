# references
# issue https://www.drupal.org/project/drupalladder/issues/3131155
# kanban tracking https://trello.com/c/Gz1NzFa2/86-when-make-changes-over-the-ladder-and-goes-offline-and-login-again-changes-get-lost

Feature: permit offline-first editing (first by guarding against lost work)
User:As a ‘variation author’
Action:I can find work I didn’t save
Benifit:So that if I accidentally close the browser, I can still go back to it and continue my effort.

Scenario: Add/edit any step information i.e. title, description, tags and code content and goes offline
Given: Logged in as authenticated user
When: When user gets back online and refreshes the page
Then: The content added should be saved locally and on refreshing the page, it should ask to restore the content.

Scenario: Add/Edit image in the media tab of any step and goes offline
Given: Logged in as authenticated user
When: When user gets back online and refreshes the page
Then: The image added/edited should be saved locally and on refreshing the page, it should ask to restore the content.

Scenario: When user is offline and adds/edits any step information i.e. title, description, tags and code content
Given: Logged in as authenticated user and user is offline
When: When user gets back online and refreshes the page
Then: The content added should be saved locally and on getting back online, it refreshes the page, it should ask to restore the content.

Scenario: When user is offline and adds/edits image in the media tab of any step
Given: Logged in as authenticated user and user is offline
When: When user gets back online and refreshes the page
Then: The image added/edited should be saved locally and on refreshing the page, it should ask to restore the content.

Scenario: When user is offline and adds/edits any step information i.e. title, description, tags and code content
Given: Logged in as authenticated user and user is offline
When: When user gets back online and refreshes the page
Then: The content added should be saved locally and on getting back online, it refreshes the page, it should ask to restore the content.

Scenario: When the user Add/edit any step information i.e. title, description, tags and code content and logs out
Given: Logged in as authenticated user
When: When user logs in and opens the ladder
Then: The content added should be saved locally and on refreshing the page, it should ask to restore the content.

Scenario:  When the user Add/Edit image in the media tab of any step and and logs out
Given: Logged in as authenticated user
When: When user logs in and opens the ladder
Then: The image added/edited should be saved locally and on refreshing the page, it should ask to restore the content.

Scenario: When the user add a new step in the existing ladder and goes offline
Given: Logged in as authenticated user
When: When user gets back online and refreshes the page
Then: The step added should be saved locally and on refreshing the page, it should ask to restore the content.

Scenario: When the user delete a new step in the existing ladder and goes offline
Given: Logged in as authenticated user
When: When user gets back online and refreshes the page
Then: The step deleted should be saved locally and on refreshing the page, it should not show the removed step.
