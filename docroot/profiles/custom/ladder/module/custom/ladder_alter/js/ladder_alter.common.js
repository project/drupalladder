(function ($, Drupal) {
  Drupal.behaviors.ladderRest = {
    attach: function (context, settings) {
      localStorage.setItem(settings.token_alias, settings.user_loggedin);
    }
  };
})(jQuery, Drupal);