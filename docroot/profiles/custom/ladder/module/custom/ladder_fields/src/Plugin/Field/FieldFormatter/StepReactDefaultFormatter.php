<?php

namespace Drupal\ladder_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'ladder_react_steps' formatter.
 *
 * @FieldFormatter(
 *   id = "ladder_react_steps",
 *   label = @Translation("Ladder React Steps"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class StepReactDefaultFormatter extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'attr_id' => 'root',
      'attr_class' => 'root',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $props = [
      'attr_id',
      'attr_class',
    ];

    foreach ($props as $prop) {
      $elements[$prop] = [
        '#type' => 'textfield',
        '#title' => $this->t('<strong>@prop</strong>', [
          '@prop' => $prop,
        ]),
        '#default_value' => $this->getSetting($prop) ? $this->getSetting($prop) : 'root',
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Displays Steps through React.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $build = [];
    $paragraph_id = $items->getEntity()->id();
    $settings = $this->getSettings();
    $node = $items->getEntity();
    $nid = 0;
    $uid = \Drupal::currentUser()->id();

    if (!empty($node)) {
      $nid = $node->id();
    }

    // Set the base URL.
    $baseURL = $_SERVER['REQUEST_SCHEME'] . '://' . \Drupal::request()->getHost() . '/';

    // Markup only requires a div with an id.
    $build['#markup'] = '<div id="' . $settings['attr_id'] . '" class="' . $settings['attr_class'] . '" data-nid="' . $nid . '" data-uid="' . $uid . '" data-url="' . $baseURL . '"></div>';

    // Attach required libraries.
    $build['#attached']['library'] = [
      'ladder_react/ladder_react',
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // Formatter is only available for fields from an steps of ladder.
    if (
      $field_definition->getTargetBundle() === 'ladder'
      && $field_definition->getTargetEntityTypeId() === 'node') {
      return TRUE;
    }

    return FALSE;
  }

}
