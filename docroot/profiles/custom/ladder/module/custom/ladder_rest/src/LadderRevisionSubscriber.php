<?php

namespace Drupal\ladder_rest;

use Drupal\Core\Database\Driver\mysql\Connection;
use Drupal\node\Entity\Node;

/**
 * LadderRevisionSubscriber service class.
 */
class LadderRevisionSubscriber {

  /**
   * Variable for connection.
   *
   * @var database
   */
  private $database;

  /**
   * Construct function of the class.
   */
  public function __construct(connection $database) {
    $this->database = $database;
  }

  /**
   * Get revision node ids.
   *
   * @param: nid: node id
   *
   * @return: array of node ids
   */
  public function getRevisions($nid = 0) {

    $query = \Drupal::entityQuery('node');
    $query->condition('status', 1);
    $query->condition('type', 'ladder');
    $query->condition('field_is_revision.value', 1);
    $query->condition('field_referenced_ladder_er.target_id', $nid);
    $entity_ids = $query->execute();

    return $entity_ids;
  }

  /**
   * To check if ladder revision exist or not.
   *
   * @param: nid: node id
   *
   * @return: TRUE: if revision exist other wise FALSE
   */
  public function checkRevisionExist($nid = 0) {

    // Get revisions of entity.
    $entity_ids = $this->getRevisions($nid);

    return !empty($result) ? TRUE : FALSE;
  }

  /**
   * To add row in tree.
   *
   * @param: nid: node id
   *
   * @param: parentNid: parent node id of node
   *
   * @param: data: array of nodes wioth child
   *
   * @return: an array with related nodes
   */
  public function addNodeRow($nid, $parentNid = 0, &$data) {

    // Update current node if revision exist
    // $nid = $nid;.
    // Load node.
    $node = Node::load($nid);

    if (!empty($node)) {

      $steps = $node->get('field_steps_er')->getValue();
      // $nid = $node->id();
      // Set node values
      $data[$nid] = ['node' => $node, 'parent' => $parentNid];

      if (!empty($steps)) {
        foreach ($steps as $child) {
          $this->addNodeRow($child['target_id'], $nid, $data);
        }
      }
    }
    else {
      $data[$nid] = [];
    }
  }

  /**
   * To buid steps tree.
   *
   * @param: elements: node
   *
   * @param: parentId: parent node id
   *
   * @return: an array of nodes with child nodes
   */
  public function buildNodeTree(array $elements, $parentId = 0) {

    $uid = 0;
    $branch = [];
    foreach ($elements as $element) {

      if (isset($element['parent']) && $element['parent'] == $parentId) {

        $nid = !empty($element['node']) ? $element['node']->id() : 0;
        $children = $this->buildNodeTree($elements, $nid);

        if ($children) {
          $element['children'] = $children;
        }
        $branch[] = $element;
      }
    }
    return $branch;
  }

}
