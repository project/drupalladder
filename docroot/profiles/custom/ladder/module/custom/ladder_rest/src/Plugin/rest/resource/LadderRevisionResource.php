<?php

namespace Drupal\ladder_rest\Plugin\rest\resource;

use Drupal\taxonomy\Entity\Term;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\node\Entity\Node;
use Drupal\media\Entity\Media;
use Drupal\Core\Session\AccountProxyInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Ladder Revision Resource.
 *
 * @RestResource(
 *   id = "ladder_revision_resource",
 *   label = @Translation("Ladder Revisions"),
 *   uri_paths = {
 *     "canonical" = "/rest-api/ladder_revision/{entity_id}",
 *     "https://www.drupal.org/link-relations/create" = "/rest-api/ladder_revision"
 *   }
 * )
 */
class LadderRevisionResource extends ResourceBase {

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('custom_rest'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to entity GET requests.
   *
   * @return: \Drupal\rest\ResourceResponse
   */
  public function get($entity_id = NULL) {

    $nodes = [];
    if (!empty($entity_id)) {

      $steps = \Drupal::service('ladder_rest.ladder.child_steps');
      $revisions = \Drupal::service('ladder_rest.ladder.revisions');
      $entity_ids = $revisions->getRevisions($entity_id);

      // Build node array.
      foreach ($entity_ids as $key => $entity_id) {
        $node = Node::load($entity_id);
        $nodes[$entity_id] = $steps->nodeInfo($node);
      }
    }

    return new JsonResponse($nodes);
  }

  /**
   * Responds to POST requests.
   *
   * Returns a list of bundles for specified entity.
   *
   * @param: data: posted node data
   *
   * @return: \Drupal\rest\ResourceResponse Throws exception expected Throws exception expected.
   *
   *   API::::::::::::::::
   *   Need to create TAG through API and after that pass Ids
   *   Need to create video entity first and than pass Id to api
   */
  public function post($data) {

    $uid = \Drupal::currentUser()->id();
    $nid = $data['stepId'];

    // Type can be LADDER_UPDATE or LADDER_ORDER_CHANGE.
    $type = isset($data['type']) ? $data['type'] : '';
    // $parent_nid = isset($data['parent_id']) ? $data['parent_id'] : 0;
    // $child_ids = isset($data['child_ids']) ? $data['child_ids'] : [];
    $revisions = \Drupal::service('ladder_rest.ladder.revisions');

    // Load node.
    if (!empty($nid)) {
      $node = Node::load($nid);
    }

    if (!empty($node)) {

      // Get if current node author.
      $author = $node->getOwnerId();
      $new_node = $node;
      // dump($author);exit();
      $add_revision = ($author != $uid) ? TRUE : FALSE;
    }

    // Check if revision exist for the same step for the author.
    if ($add_revision) {

      // Create duplicate node.
      $new_node = $node->createDuplicate();

      $new_node->set('field_is_revision', 1);
      $new_node->set('uid', $uid);
    }

    // "type":[{"target_id":"ladder"}]
    // ,"title":[{"value":"iPhone — Hear what you read — Apple2"}]
    // ,"field_is_ladder":[{"value":true}]
    // ,"field_start_time":[{"value":"PT0H0M2S"}]
    // ,"field_end_time":[{"value":"PT0H0M2S"}]
    // ,"field_tags":[],
    // "field_remote_video_er":[{"target_id":336}]
    // Update fields related values.
    $this->setNodeValues($new_node, $data);

    // Add entry into custom table.
    if ($add_revision) {
      // $fields = ['source_nid' => $node->id(),
      // 'dest_nid' => $new_node->id(), 'uid' => $uid];
      // \Drupal::database()->insert('dl_default_node')->fields($fields)->execute();
      $new_node->set('field_referenced_ladder_er', $node->id());
    }

    $new_steps = $exist_child_ids = $child_nodes = $new_tags = [];
    // Manage childs if exist.
    if (!empty($node)) {
      $steps = $node->get('field_steps_er')->getValue();

      // Steps.
      foreach ($steps as $child) {
        $exist_child_ids[] = $child['target_id'];
      }

      $steps = $exist_child_ids;

      // If ($type == 'LADDER_ORDER_CHANGE') {
      // $steps = $child_ids;
      // }.
      $child_nodes = $data['children'];

      if ($add_revision) {

        // @todo - check if node is new or not
        // Create new node if node does not exist
        foreach ($child_nodes as $key => $step) {

          $images = $step['images'];
          $step['images'] = [];
          foreach ($images as $key => $image) {

            if (isset($image['is_new'])) {
              $step['images'][] = $this->createImage($image);
            }
          }

          $videoId = $step['videoId'];
          $step['videoId'] = [];
          if (isset($videoId['is_new'])) {
            $step['videoId'][] = $this->createRemoteVideo($videoId);
          }

          $tags = $step['tags'];
          $step['tags'] = [];
          foreach ($tags as $key => $tag) {
            $step['tags'][] = $this->getTermData($tag);
          }

          $stepId = $step['stepId'];
          if (isset($step['is_new'])) {
            $new_steps[] = $this->createChildSteps($node, $step);
          }
          else {
            $new_steps[] = $this->cloneChildSteps($node, $stepId);
          }
        }
      }

      else {
        $new_steps = $steps;
      }
    }

    $new_nid = '';
    $new_node->set('field_steps_er', $new_steps);
    $new_node->save();

    $new_nid = $new_node->id();
    $result = ['nid' => $new_nid];
    return new ResourceResponse($result);
  }

  /**
   * To set node field values.
   *
   * @param: new_node: ladder node object
   *
   * @param: data: posted node data
   */
  public function setNodeValues(&$new_node, $data) {

    // Set node field values if set.
    if (isset($data['title']) && !empty($data['title'])) {
      $new_node->set('title', $data['title']);
    }

    if (isset($data['description']) && !empty($data['description'])) {
      $new_node->set('body', $data['body']);
    }

    if (isset($data['field_is_ladder']) && !empty($data['field_is_ladder'])) {
      $new_node->set('field_is_ladder', $data['field_is_ladder']['value']);
    }

    if (isset($data['field_start_time']) && !empty($data['field_start_time'])) {
      $new_node->set('field_start_time', $data['field_start_time']);
    }

    if (isset($data['field_end_time']) && !empty($data['field_end_time'])) {
      $new_node->set('field_end_time', $data['field_end_time']);
    }

    if (isset($data['field_tags']) && !empty($data['field_tags'])) {
      // @todo - create new tags if needed
      $new_node->set('field_tags', $data['field_tags']);
    }

    if (isset($data['field_remote_video_er']) && !empty($data['field_remote_video_er'])) {
      // @todo - create new video entity if needed
      $new_node->set('field_remote_video_er', $data['field_remote_video_er']);
    }

    // @todo - screenshot
    // if (isset($data['field_remote_video_er']) &&
    // !empty($data['field_remote_video_er'])) {
    // @todo - create new screenshot entity if needed
    // $new_node->set('field_remote_video_er', $data['field_remote_video_er']);
    // }
  }

  /**
   * To clone child node.
   *
   * @param: parent_node: parent node
   *
   * @param: nid: current nodes
   *
   * @return: nid of cloned node
   */
  public function cloneChildSteps($parent_node = [], $nid = []) {

    $node = Node::load($nid);

    $new_node = $this->cloneNode($node);

    if (!empty($node)) {
      $steps = $node->get('field_steps_er')->getValue();

      if (!empty($steps)) {
        foreach ($steps as $child) {
          $new_steps[] = $this->cloneChildSteps($new_node, $child['target_id']);
        }
      }

      if (!empty($new_steps)) {
        $new_node->set('field_steps_er', $new_steps);
      }
      $new_node->save();
      return $new_node->id();
    }
  }

  /**
   * To clone child node.
   *
   * @param: parent_node: parent node
   *
   * @param: data: create nodes
   *
   * @return: nid of created node
   */
  public function createChildSteps($parent_node = [], $data = []) {

    $uid = \Drupal::currentUser()->id();
    $nid = '';
    $new_node = Node::create([
      'type' => $data['type'],
      'title' => $data['title'],
      'body' => $data['description'],
      'field_tags' => $data['tags'],
      'field_remote_video_er' => $data['videoId'],
      'field_image_er' => $data['images'],
      'uid' => $uid,
    ]);
    $new_node->save();
    return $new_node->id();

  }

  /**
   * To create images.
   *
   * @param: image: create images.
   *
   * @return: mid of created media image.
   */
  public function createImage($image = []) {

    $img = base64_decode($image['data'][0]['value']);
    $path = $image['_links']['type']['href'];
    $imgName = $image['filename'][0]['value'];
    $file_image = file_save_data($img, 'public://' . $imgName, FILE_EXISTS_REPLACE);

    $mid = '';
    $media = Media::create([
      'bundle'      => 'image',
      'uid'         => \Drupal::currentUser()->id(),
      'name'        => $image['name'],
      'field_media_image' => [
        'target_id' => $file_image->id(),
        'alt' => $image['name'],
      ],
    ]);
    $media->save();
    return $media->id();
  }

  /**
   * To create remote video.
   *
   * @param: videoId: create video
   *
   * @return: mid of created remote video
   */
  public function createRemoteVideo($videoId = []) {

    $mid = '';
    $media = Media::create([
      'bundle'      => 'remote_video',
      'uid'         => \Drupal::currentUser()->id(),
      'name'        => $videoId['name'],
      'field_media_oembed_video' => [
        'value' => $videoId['youtubeId'],
      ],
    ]);
    $media->save();
    return $media->id();
  }

  /**
   * To get taxonomy term.
   *
   * @param: tag: data of taxonomy term
   *
   * @return: tid of gotten taxonomy term
   */
  public function getTermData($tag = []) {

    if (isset($tag['is_new'])) {

      $tid = db_select('taxonomy_term_field_data', 't')
        ->fields('t', ['tid'])
        ->condition('vid', 'tags', '=')
        ->condition('name', $tag['name'], '=')
        ->execute()
        ->fetchField();
      if ($tid) {
        $step['tags'] = $tid;
      }
      else {
        $step['tags'] = $this->createNewTags($tag);
      }
    }
    else {
      $step['tags'] = $tag['id'];
    }
    return $step['tags'];
  }

  /**
   * To create taxonomy term.
   *
   * @param: tag: create taxonomy term
   *
   * @return: tid of created taxonomy term
   */
  public function createNewTags($tag = []) {

    $term = Term::create([
      'vid' => 'tags',
      'name' => $tag['name'],
    ]);
    $term->save();
    return $term->id();
  }

  /**
   * To clone node.
   *
   * @param: node: node
   *
   * @return: object of new node
   */
  public function cloneNode($node = []) {

    $new_node = [];
    if (!empty($node)) {

      $uid = \Drupal::currentUser()->id();

      $new_node = $node->createDuplicate();
      $new_node->set('title', $node->getTitle() . ' - r');
      $new_node->set('field_is_revision', 1);
      $new_node->set('uid', $uid);

      $new_node->save();
    }

    return $new_node;
  }

}
