<?php

namespace Drupal\ladder_rest\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides a Ladder Resource.
 *
 * @RestResource(
 *   id = "ladder_resource",
 *   label = @Translation("Ladder Resource"),
 *   uri_paths = {
 *     "canonical" = "/rest-api/ladder/{entity_id}"
 *   }
 * )
 */
class LadderResource extends ResourceBase {

  /**
   * Responds to entity GET requests.
   *
   * @return: \Drupal\rest\ResourceResponse
   */
  public function get($entity_id = NULL) {

    $rows = $entityResult = [];
    if (!empty($entity_id)) {
      // To check the ladder is the main ladder.
      $query = \Drupal::entityQuery('node');
      $query->condition('status', 1);
      $query->condition('nid', $entity_id);
      // $query->condition('field_is_ladder', 1);
      $entityResult = $query->execute();
    }

    if (!empty($entityResult)) {

      $steps = \Drupal::service('ladder_rest.ladder.child_steps');

      /*We call add row once and then it's called recursively to fetch each child.*/
      $steps->addRow($entity_id, "0", $data);

      // Now turn the flat data into a multi-dimensional array.
      $rows = $steps->buildTree($data);
    }

    // And inspect our tree.
    return new JsonResponse($rows);
  }

}
